<?php

use App\Scheduler\Kernel;
use Carbon\Carbon;

use App\Events\SomeEvent;
use App\Events\UpdateRatesEvent;
use App\Events\ShopifyProductsSyncEvents;
use App\Events\OberloProductsSyncEvent;
use App\Events\SyncOrdersEvent;
use App\Events\OberloLowInventoryProductsSyncEvent;

require_once 'vendor/autoload.php';
require_once 'config/container.php';

$kernel = new Kernel;

$kernel->setDate(Carbon::now()->tz('Europe/London'));

//$kernel->add(new UpdateRatesEvent($container))->everyMinute();
//$kernel->add(new ShopifyProductsSyncEvents($container))->everyMinute();
//$kernel->add(new OberloProductsSyncEvent($container))->everyMinute();
//$kernel->add(new SyncOrdersEvent($container))->everyMinute();
$kernel->add(new OberloLowInventoryProductsSyncEvent($container))->everyMinute();

$kernel->run();