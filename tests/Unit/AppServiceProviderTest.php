<?php

use Dotenv\Dotenv;
use PHPUnit\Framework\TestCase;

class AppServiceProviderTest extends TestCase {

  /**  @test */
  public function service_has_shopify_api_key_environment_variable() {
    
    $dotenv = new Dotenv(__DIR__ . '\..\\..\\');
    $dotenv->load();

    $this->assertTrue(isset($_ENV['SHOPIFY_API_KEY']));

  }

  /**  @test */
  public function service_has_shopify_api_password_environment_variable() {
    
    $dotenv = new Dotenv(__DIR__ . '\..\\..\\');
    $dotenv->load();

    $this->assertTrue(isset($_ENV['SHOPIFY_API_PASSWORD']));

  }

  /**  @test */
  public function service_has_shopify_api_shared_environment_variable() {
    
    $dotenv = new Dotenv(__DIR__ . '\..\\..\\');
    $dotenv->load();

    $this->assertTrue(isset($_ENV['SHOPIFY_API_SHARED_SECRET']));

  }

  /**  @test */
  public function service_has_shopify_shop_environment_variable() {
    
    $dotenv = new Dotenv(__DIR__ . '\..\\..\\');
    $dotenv->load();

    $this->assertTrue(isset($_ENV['SHOPIFY_HOST']));

  }

  /**  @test */
  public function service_has_oberlo_environment_variables() {
    
    $dotenv = new Dotenv(__DIR__ . '\..\\..\\');
    $dotenv->load();

    $this->assertTrue(isset($_ENV['OBERLO_BEARER']));

  }

}