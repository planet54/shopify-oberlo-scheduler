<?php 

try {

  $dotenv = (new Dotenv\Dotenv(__DIR__ . '/../'))->load();

} catch(Dotenv\Exception\InvalidPathException $e) {}

$container = new League\Container\Container;

$container->addServiceProvider(new App\Providers\AppServiceProvider());