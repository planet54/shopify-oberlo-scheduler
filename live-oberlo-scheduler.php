<?php

use App\Scheduler\Kernel;
use Carbon\Carbon;

use App\Events\OberloProductsSyncEvent;
use App\Events\OberloLowInventoryProductsSyncEvent;

require_once 'vendor/autoload.php';
require_once 'config/container.php';

$kernel = new Kernel;

$kernel->add(new OberloLowInventoryProductsSyncEvent($container))->everyFiveMinutes();
$kernel->add(new OberloProductsSyncEvent($container))->everyTenMinutes();

$kernel->run();