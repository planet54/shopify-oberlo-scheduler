<?php

namespace App\Providers;

use League\Container\ServiceProvider\AbstractServiceProvider;

use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use GuzzleHttp\Client;

class AppServiceProvider extends AbstractServiceProvider
{
  protected $provides = [
    'settings',
    'firebase',
    'exchange'
  ];

  public function register ()
  {

    $container = $this->getContainer();

    $container->share('settings', function() {
      return [
        "shopify" => [
          'api_key' => $_ENV['SHOPIFY_API_KEY'] ? $_ENV['SHOPIFY_API_KEY'] : "",
          'api_password' => $_ENV['SHOPIFY_API_PASSWORD'] ? $_ENV['SHOPIFY_API_PASSWORD'] : "",
          'shared_key' => $_ENV['SHOPIFY_API_SHARED_SECRET'] ? $_ENV['SHOPIFY_API_SHARED_SECRET'] : "",
          'shop' => $_ENV['SHOPIFY_HOST'] ? $_ENV['SHOPIFY_HOST'] : "", 
        ],
        "oberlo" => $_ENV['OBERLO_BEARER'] ? $_ENV['OBERLO_BEARER'] : "",
      ];
    });

    $container->share('firebase', function () {
    
      $serviceAccount = ServiceAccount::fromJsonFile(__DIR__ . '/../../firebase_auth.json');
  
      return (new Factory)
      ->withServiceAccount($serviceAccount)
      ->withDatabaseUri('https://plant54-product-sync.firebaseio.com')
      ->create();
  
    });

    $container->share('exchange', function () {

      $client = new Client();

      $res = $client->request('GET', 'http://apilayer.net/api/live',
        [
          'query' => [
            'access_key' => $_ENV['CURRENCY_LAYER_API_KEY'] ? $_ENV['CURRENCY_LAYER_API_KEY'] : ""
          ]
        ]
      );

      return json_decode($res->getBody()->getContents());

    });

  
  }
}