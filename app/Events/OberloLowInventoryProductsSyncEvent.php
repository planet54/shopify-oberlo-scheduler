<?php

namespace App\Events;

use PHPShopify\ShopifySDK;
use GuzzleHttp\Client;
use App\Scheduler\Event;

class OberloLowInventoryProductsSyncEvent extends Event
{
  private $config;
  private $oberlo;
  protected $container;
  protected $datebase;
  protected $shopify;

  function __construct($container)
  {
    $this->container = $container;
    $settings = $this->container->get('settings');
    $firebase = $this->container->get('firebase');
    $shopify = $settings["shopify"];

    $this->database = $firebase->getDatabase();
    $this->oberlo = $settings["oberlo"];
    $this->config = array(
      'ShopUrl' => $shopify['shop'],
      'ApiKey' => $shopify['api_key'],
      'Password' => $shopify['api_password'],
    );
    
    $this->shopify = ShopifySDK::config($this->config);

  }

  public function handle()
  {

    $this->getProducts();
    
  }

  public function getProducts()
  {
    $client = new Client();

    $items_per_page = 10;
    $total_items =  200;
    $page = 1;
    $pages = ceil($total_items/$items_per_page);
    $reference = $this->database->getReference('shopify/products/');
    $productVariants = $reference->getValue();

    try 
    {

      $res = $client->request('GET', 'https://supply.oberlo.com/supplier/api/v1/products',
        [
          'headers' => [
            'Authorization' => ['Bearer '. $this->oberlo]
          ],
          'query' => [
            'page' => $page
          ]
        ]
      );
          
      $oberloResponse = json_decode($res->getBody()->getContents(), true);

      if(!isset($oberloResponse) || $oberloResponse['success'] === false || $oberloResponse == null || count($oberloResponse['data']['items']) === 0) return false;
      
      $pagination = $oberloResponse['data']['pagination'];
      $items_per_page = $pagination['items_per_page'];
      $total_items =  $pagination['total_items'];
      $pages = ceil($total_items/$items_per_page);

      foreach ($oberloResponse['data']['items'] as $product) {
         foreach ($product['variants'] as $variant) {
           if(!isset($productVariants[$variant["sku"]])) {
            $product = $this->shopify->Product($product['parent_sku'])->get();
            $this->update_oberlo_variant_stock($variant, $product['variants']);
           }
          
           if($variant["stock"] === 0) {
            $this->unpublish_oberlo_variant_stock($variant["sku"]);
           }
        }
      }

      $page = 2;

      while ($page <= $pages) {
       $this->get_update_oberlo_products($page, $productVariants);
       $page++;
      }

      return $oberloResponse['data']['items'];
      
    } 
    
    catch (\GuzzleHttp\Exception\ClientException $exception) 
    {
      return false;
    }

    catch (\GuzzleHttp\Exception\RequestException $exception)
    {

      return false;

    }

  }

  public function update_oberlo_variant_stock(array $oberloVariant,array $shopifyVariants)
  {

    foreach ($shopifyVariants as $variant) {

      if($oberloVariant['sku'] === $variant['sku']) {
        $this->updateStock($variant['sku'], $variant['inventory_quantity'] );
      }
  
    }

  }

  public function unpublish_oberlo_variant_stock(string $sku)
  {
    try 
    {
      $client = new Client;

      $res = $client->request('PUT', 'https://supply.oberlo.com/supplier/api/v1/product-variants/'.$sku . '/unpublish',
        [
          'headers' => [
            'Authorization' => ['Bearer '. $this->oberlo],
            'Content-Type' => 'application/json',
            'Accept' => 'application/json'
          ]
        ]
      );
          
      $oberloResponse = json_decode($res->getBody()->getContents(), true);
      //var_dump($oberloResponse);
      if(!isset($oberloResponse) || $oberloResponse['success'] === false || $oberloResponse == null) return false;

      return $oberloResponse['data'];
      
    } 
    
    catch (\GuzzleHttp\Exception\ClientException $exception) 
    {
      return false;
    }

    catch (\GuzzleHttp\Exception\RequestException $exception)
    {

      return false;

    }

  }

  function updateStock(string $sku, int $stock) 
  {

    try 
    {
      $client = new Client;

      $res = $client->request('POST', 'https://supply.oberlo.com/supplier/api/v1/product-variants/'.$sku.'/stock',
        [
          'headers' => [
            'Authorization' => ['Bearer '. $this->oberlo],
            'Content-Type' => 'application/json',
            'Accept' => 'application/json'
          ],
          'body' => json_encode([
            'stock' => $stock
          ])
        ]
      );
          
      $oberloResponse = json_decode($res->getBody()->getContents(), true);
      
      if(!isset($oberloResponse) || $oberloResponse['success'] === false || $oberloResponse == null) return false;

      return $oberloResponse['data'];
      
    } 
    
    catch (\GuzzleHttp\Exception\ClientException $exception) 
    {
      return false;
    }

    catch (\GuzzleHttp\Exception\RequestException $exception)
    {

      return $this->updateStock($sku, $stock);

    }

  }

  function get_update_oberlo_products(int $page, array $productVariants)
  {
    $client = new Client;
    try 
    {

      $res = $client->request('GET', 'https://supply.oberlo.com/supplier/api/v1/products',
        [
          'headers' => [
            'Authorization' => ['Bearer '. $this->oberlo]
          ],
          'query' => [
            'page' => $page
          ]
        ]
      );
          
      $oberloResponse = json_decode($res->getBody()->getContents(), true);

      if(
        !isset($oberloResponse) || 
        $oberloResponse['success'] === false || 
        $oberloResponse == null || 
        count($oberloResponse['data']['items']) === 0
        ) return false;

      foreach ($oberloResponse['data']['items'] as $product) {
         foreach ($product['variants'] as $variant) {
           if(!isset($productVariants[$variant["sku"]])) {
            $product = $this->shopify->Product($product['parent_sku'])->get();
            $this->update_oberlo_variant_stock($variant, $product['variants']);
           }
           if($variant["stock"] === 0) {
            $this->unpublish_oberlo_variant_stock($variant["sku"]);
           }
        }
      }

      return $oberloResponse['data']['items'];
      
    } 
    
    catch (\GuzzleHttp\Exception\ClientException $exception) 
    {
      return false;
    }

    catch (\GuzzleHttp\Exception\RequestException $exception)
    {

      return $this->get_update_oberlo_products($page, $productVariants);

    }

  }

}
