<?php

namespace App\Events;

use PHPShopify\ShopifySDK;
use GuzzleHttp\Client;
use App\Scheduler\Event;

class SyncOrdersEvent extends Event
{

  private $oberlo;
  protected $container;
  protected $datebase;
  protected $rate;
  protected $database;
  protected $shopify;

  function __construct($container)
  {

    $this->container = $container;
    $settings = $container->get('settings');
    $firebase = $this->container->get('firebase');
    $this->database = $firebase->getDatabase();
    $reference = $this->database->getReference('helpers');
    $helpers = $reference->getValue();
    $this->rate = $helpers["exchange"]["rates"]["USDZAR"];
    
    $shopify = $settings["shopify"];

    $this->config = array(
      'ShopUrl' => $shopify['shop'],
      'ApiKey' => $shopify['api_key'],
      'Password' => $shopify['api_password'],
    );
    
    $this->shopify = ShopifySDK::config($this->config);

    $this->oberlo = $settings["oberlo"];

  }

  public function handle() 
  {

    $this->handle_orders_to_shopify();
    $this->handle_oberlo_order_status();
 
  }

  function handle_oberlo_order_status()
  {
    $pagination = $this->get_pagination_for_oberlo_in_processing_orders();

    $orders = [];
    $items_per_page = $pagination['items_per_page'];
    $total_items =  $pagination['total_items'];
    $page = 1;
    $pages = ceil($total_items/$items_per_page);

    while ($page <= $pages) {
      $o = $this->get_in_proccess_oberlo_orders(["page" => $page]);
      if($o) {
        $orders[] = $o;
      }
      $page++;
    }

    if(count($orders) === 0) return false;
    
    $orders = $this->flatten_array($orders);

    foreach ($orders as $key => $order) {
      
      $this->check_order_statuses($order);

    }

    return $orders;
  
  }

  function check_order_statuses(array $order)
  {

    $reference = $this->database->getReference('shopify/orders/' . $order['id']);
    $orderRefs = $reference->getSnapshot()->getValue();
    $ShopOrder = $this->shopify->Order($orderRefs['order_number'])->get();

    if($ShopOrder['fulfillment_status'] === "fulfilled") {
      $this->update_oberlo_order_fullfilment($order, $ShopOrder);
    }
    
  }

  function handle_orders_to_shopify()
  {
    $pagination = $this->get_pagination_for_oberlo_unfullfilled_orders();

    $orders = [];
    $items_per_page = $pagination['items_per_page'];
    $total_items =  $pagination['total_items'];
    $page = 1;
    $pages = ceil($total_items/$items_per_page);

    while ($page <= $pages) {
      $orders[] = $this->get_unfullfilled_oberlo_orders(["page" => $page]);
      $page++;
    }

    $orders = $this->flatten_array($orders);
    $reference = $this->database->getReference('shopify/orders/');
    $orderRefs = $reference->getSnapshot()->getValue();

    foreach ($orders as $key => $order) {

      if(!isset($orderRefs[$order['id']])) {
        $this->create_shopify_orders($order);
      }

    }

    return $orders;

  }
  function update_oberlo_order_fullfilment(array $order, array $shopOrder)
  {

    $items = [];

    foreach ($order["items"] as $item) {
      $items[] = [
        "order_item_id" => $item["id"],
        "quantity" => $item["quantity"]
      ];
    }

    
    $client = new Client();
    
    $res = $client->request('POST', 'https://supply.oberlo.com/supplier/api/v1/orders/'.$order['id'].'/fulfillments',
        [
          'headers' => [
            'Authorization' => ['Bearer '. $this->oberlo],
            "Content-Type" =>"application/json"
          ],
          'body' => json_encode([
            "tracking_number" => $shopOrder["fulfillments"][0]["tracking_number"],
            "package_carrier_id" => 100006,
            "items" => $items
          ])
        ]
      );
          
      $oberloResponse = json_decode($res->getBody()->getContents(), true);

      if(!isset($oberloResponse) || $oberloResponse['success'] === false || $oberloResponse == null) return false;

    return $oberloResponse['data'];
      

  }

  function create_shopify_orders(array $order)
  {
    $buyersName = explode(' ', $order['buyer_name']);

    $shopifyOrder = [
      "line_items" => $this->sort_order_items_for_shopify($order['items']),
      "customer"=>[
        "first_name" => $buyersName[0],
        "last_name"=> $buyersName[1]
      ],
      "billing_address" => [
        "first_name"=> $buyersName[0],
        "last_name"=> $buyersName[1],
        "address1"=> $order['shipping_address_line_1'],
        "address2"=> $order['shipping_address_line_2'],
        "phone"=> $order['buyer_phone'],
        "city"=> $order['shipping_address_city'],
        "province"=> $order['shipping_address_province'],
        "country"=> $order['shipping_address_country'],
        "zip"=> $order['shipping_address_postcode']
      ],
      "shipping_address" => [
        "first_name"=> $buyersName[0],
        "last_name"=> $buyersName[1],
        "address1"=> $order['shipping_address_line_1'],
        "address2"=> $order['shipping_address_line_2'],
        "phone"=> $order['buyer_phone'],
        "city"=> $order['shipping_address_city'],
        "province"=> $order['shipping_address_province'],
        "country"=> $order['shipping_address_country'],
        "zip"=> $order['shipping_address_postcode']
      ],
      "shipping_lines" => [
        [
          "code"=>"INT.TP",
          "price"=> $this->rate * $order['shipping_rate'],
          "price_set" => [
            "shop_money" => [
              "amount" => $this->rate * $order['shipping_rate'],
              "currency_code" => "ZAR"
            ]
          ],
          "title" => $order['shipping_rate_name'],
          "carrier_identifier" => strtolower($order['shipping_rate_name'])
        ]
      ], 
      "note"=> "Eretailer Oberlo - Order number: ". $order['id'],
      "tags"=> "dropshipping, eretailer, oberlo",
      "inventory_behaviour"=> "decrement_obeying_policy",
      "fulfillment_status" => "unfulfilled"
    ];

    $shopifyCreateOrder = $this->shopify->Order->post($shopifyOrder);

    $reference = $this->database->getReference('shopify/orders/'. $order['id']);
    $reference->set([
      "order_number"=>$shopifyCreateOrder['id']
    ]);

    $client = new Client();

    try 
    {

      $res = $client->request('POST', 'https://supply.oberlo.com/supplier/api/v1/orders/'.$order['id'].'/process',
        [
          'headers' => [
            'Authorization' => ['Bearer '. $this->oberlo]
          ]
        ]
      );
          
      $oberloResponse = json_decode($res->getBody()->getContents(), true);

      if(!isset($oberloResponse) || $oberloResponse['success'] === false || $oberloResponse === null) return false;

      return $oberloResponse['data'];
      
    } 
    
    catch (\GuzzleHttp\Exception\ClientException $exception) 
    {
      return false;
    }

    catch (\GuzzleHttp\Exception\RequestException $exception)
    {
      return false;
    }

  }

  function sort_order_items_for_shopify(array $order_items)
  {
    $items = [];

    foreach ($order_items as $item) {
     $items[] = $this->getProduct($item);
    }

    return $items;

  }

  function get_in_proccess_oberlo_orders(array $params = ["page" => 1])
  {

    $client = new Client();

    try 
    {

      $res = $client->request('GET', 'https://supply.oberlo.com/supplier/api/v1/orders',
        [
          'headers' => [
            'Authorization' => ['Bearer '. $this->oberlo]
          ],
          'query' => [
            'page' => $params['page'],
            'fulfillment_status' => 'in_processing',
            'payment_status' => 'paid',
            'other'=> 'not_shipped'
          ]
        ]
      );
          
      $oberloResponse = json_decode($res->getBody()->getContents(), true);

      if(!isset($oberloResponse) || $oberloResponse['success'] === false || $oberloResponse == null) return false;

      return $oberloResponse['data']['items'];
      
    } 
    
    catch (\GuzzleHttp\Exception\ClientException $exception) 
    {
      return false;
    }

    catch (\GuzzleHttp\Exception\RequestException $exception)
    {

      return $this->get_in_proccess_oberlo_orders(["page" => $params['page']]);

    }

  }

  function get_unfullfilled_oberlo_orders(array $params = ["page" => 1])
  {

    $client = new Client();

    try 
    {

      $res = $client->request('GET', 'https://supply.oberlo.com/supplier/api/v1/orders',
        [
          'headers' => [
            'Authorization' => ['Bearer '. $this->oberlo]
          ],
          'query' => [
            'page' => $params['page'],
            'fulfillment_status' => 'not_fulfilled',
            'payment_status' => 'paid',
            'other'=> 'not_shipped'
          ]
        ]
      );
          
      $oberloResponse = json_decode($res->getBody()->getContents(), true);

      if(!isset($oberloResponse) || $oberloResponse['success'] === false || $oberloResponse === null) return false;

      return $oberloResponse['data']['items'];
      
    } 
    
    catch (\GuzzleHttp\Exception\ClientException $exception) 
    {
      return false;
    }

    catch (\GuzzleHttp\Exception\RequestException $exception)
    {

      return $this->get_unfullfilled_oberlo_orders(["page" => $params['page']]);

    }

  }

  function getProduct(array $orderItem)
  {
    $client = new Client();

    try 
    {
      $credentials = base64_encode( $this->config['ApiKey'] . ':' .$this->config['Password']);
      $res = $client->request('GET', 'https://'. $this->config['ShopUrl'].'/admin/products.json',
        [
          'headers' => [
            'Authorization' => ['Basic '. $credentials]
          ],
          'query' => [
            'title' => $orderItem['product_title']
          ]
        ]
      );


      $products = json_decode($res->getBody()->getContents(), true)['products'];

      foreach ($products as $product) {
        $productVariant = false;
        $has_sku = false;

        if( $orderItem['sku'] === '39498307409' ) $orderItem['sku'] = '39498307409';
        
        foreach($product['variants'] as $variant) {

          if($variant['sku'] === $orderItem['sku']) {
            $productVariant = $variant;
            $has_sku = true;
            break;
          }
        }

        if($has_sku) {
            return [
              'variant_id' => $productVariant['id'],
              'quantity' => $orderItem['quantity'],
              'price' => $productVariant['price']
            ];
          
          break;
        }

      }
      return [];

      
    } 
    
    catch (\GuzzleHttp\Exception\ClientException $exception) {}

    catch (\GuzzleHttp\Exception\RequestException $exception)
    {
    }
  }
  function get_pagination_for_oberlo_unfullfilled_orders()
  {
    $client = new Client();
    try 
    {

      $res = $client->request('GET', 'https://supply.oberlo.com/supplier/api/v1/orders',
        [
          'headers' => [
            'Authorization' => ['Bearer '. $this->oberlo]
          ],
          'query' => [
            'page' => 1,
            'fulfillment_status' => 'not_fulfilled',
            'payment_status' => 'paid',
            'other'=> 'not_shipped'
          ]
        ]
      );
          
      $productResponse = json_decode($res->getBody()->getContents(), true);

      if(!isset($productResponse) || $productResponse['success'] === false) return;

      return $productResponse['data']['pagination'];
      
    } 
    
    catch (\GuzzleHttp\Exception\ClientException $exception) {}

    catch (\GuzzleHttp\Exception\RequestException $exception)
    {

      return $this->getProductCount();

    }

  }
  function get_pagination_for_oberlo_in_processing_orders()
  {
    $client = new Client();
    try 
    {

      $res = $client->request('GET', 'https://supply.oberlo.com/supplier/api/v1/orders',
        [
          'headers' => [
            'Authorization' => ['Bearer '. $this->oberlo]
          ],
          'query' => [
            'page' => 1,
            'fulfillment_status' => 'in_processing',
            'payment_status' => 'paid',
            'other'=> 'not_shipped'
          ]
        ]
      );
          
      $productResponse = json_decode($res->getBody()->getContents(), true);

      if(!isset($productResponse) || $productResponse['success'] === false) return;

      return $productResponse['data']['pagination'];
      
    } 
    
    catch (\GuzzleHttp\Exception\ClientException $exception) {}

    catch (\GuzzleHttp\Exception\RequestException $exception)
    {

      return $this->getProductCount();

    }

  }

  function flatten_array(array $items)
  {
    $flattened = [];
    $index = 0;
    foreach ($items as $key => $item) {
      foreach ($item as $item_item) {
        $flattened[] = $item_item;
        $index++;
      }      
    }
    return $flattened;
  }

}