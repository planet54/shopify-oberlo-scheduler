<?php

namespace App\Events;

use GuzzleHttp\Client;
use App\Scheduler\Event;

class OberloProductsSyncEvent extends Event
{

  private $oberlo;
  protected $container;
  protected $datebase;

  function __construct($container)
  {
    $this->container = $container;
    $settings = $container->get('settings');
    $firebase = $this->container->get('firebase');
    $this->database = $firebase->getDatabase();
    $this->oberlo = $settings["oberlo"];

  }

  public function handle() 
  {
    $pagination = $this->getPagination();

    $products = [];
    $items_per_page = $pagination['items_per_page'];
    $total_items =  $pagination['total_items'];
    $page = 1;
    $pages = ceil($total_items/$items_per_page);
    
  }
 
  function unpublishProduct(array $product)
  {
    
  }
  function getProductVariant(string $sku) 
  {
    $client = new Client();
    try 
    {

      $res = $client->request('GET', 'https://supply.oberlo.com/supplier/api/v1/product-variants/' . $sku ,
        [
          'headers' => [
            'Authorization' => ['Bearer '. $this->oberlo],
            'Accept' => 'application/json',
            'Content-Type' => 'application/json'
          ]
        ]
      );
          
      $oberloResponse = json_decode($res->getBody()->getContents(), true);

      if(!isset($oberloResponse) || $oberloResponse['success'] === false || $oberloResponse === null) return false;
  
      return $oberloResponse['data'];
      
    } 
    
    catch (\GuzzleHttp\Exception\ClientException $exception) 
    {
      return false;
    }

    catch (\GuzzleHttp\Exception\RequestException $exception)
    {

      return $this->getProductVariant();

    }
  }

  function getProducts(array $params = ["page" => 1])
  {

    $client = new Client();

    try 
    {

      $res = $client->request('GET', 'https://supply.oberlo.com/supplier/api/v1/products',
        [
          'headers' => [
            'Authorization' => ['Bearer '. $this->oberlo]
          ],
          'query' => [
            'page' => $params['page']
          ]
        ]
      );
          
      $productResponse = json_decode($res->getBody()->getContents(), true);

      if(!isset($productResponse) || $productResponse['success'] === false) return;

      return $productResponse['data']['items'];

    } 
    
    catch (\GuzzleHttp\Exception\ClientException $exception) {}

    catch (\GuzzleHttp\Exception\RequestException $exception)
    {

      return $this->getProducts(['page'=> $params['page']]);

    }

  }

  function getPagination()
  {
    $client = new Client();
    try 
    {

      $res = $client->request('GET', 'https://supply.oberlo.com/supplier/api/v1/products',
        [
          'headers' => [
            'Authorization' => ['Bearer '. $this->oberlo]
          ],
          'query' => [
            'page' => 1
          ]
        ]
      );
          
      $productResponse = json_decode($res->getBody()->getContents(), true);

      if(!isset($productResponse) || $productResponse['success'] === false) return;

      return $productResponse['data']['pagination'];
      
    } 
    
    catch (\GuzzleHttp\Exception\ClientException $exception) {}

    catch (\GuzzleHttp\Exception\RequestException $exception)
    {

      return $this->getProductCount();

    }

  }

  function flatten_array(array $items)
  {
    $flattened = [];
    $index = 0;
    foreach ($items as $key => $item) {
      foreach ($item as $item_item) {
        $flattened[] = $item_item;
        $index++;
      }      
    }
    return $flattened;
  }

}
