<?php

namespace App\Events;

use App\Scheduler\Event;
use Carbon\Carbon;

class UpdateRatesEvent extends Event 
{
  protected $container;
  protected $datebase;

  function __construct($container)
  {

    $this->container = $container;
    $firebase = $this->container->get('firebase');
    $this->database = $firebase->getDatabase();

  }

  public function handle() 
  {

    if(!$this->isRatesDayOld()) return;

    $this->setRates();

  }

  function isRatesDayOld()
  {
    $reference = $this->database->getReference('helpers');
    $helpers = $reference->getValue();

    if(!isset($helpers) ) return true;
    
    $date = Carbon::createFromTimestamp($helpers["exchange"]["timestamp"]);
   
    return Carbon::parse(Carbon::parse($date)->addDays(1))->lt(Carbon::now());

  }

  function setRates()
  {
    $exchange = $this->container->get('exchange');

    if(!$exchange->success) return;
      
    $this->database->getReference('helpers/exchange')
    ->set([
        'timestamp'=> Carbon::parse($exchange->timestamp)->addDays(1)->timestamp,
        'rates' => $exchange->quotes,
    ]);
   
  }

}