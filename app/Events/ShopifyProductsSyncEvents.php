<?php

namespace App\Events;

use App\Scheduler\Event;
use PHPShopify\ShopifySDK;
use Carbon\Carbon;
use GuzzleHttp\Client;

require_once __DIR__ . '/../../sample-data.php';

class ShopifyProductsSyncEvents extends Event
{
  private $config;
  private $database;
  protected $shopify;
  protected $page = 1;
  protected $totalPages = 1;
  protected $productCount = 0;
  protected $limit = 250;
  protected $rate = 1;

  function __construct($container)
  {
    $settings = $container->get('settings');
    $firebase = $container->get('firebase');

    $this->database = $firebase->getDatabase();
    $reference = $this->database->getReference('helpers');
    $helpers = $reference->getValue();
    $this->rate = $helpers["exchange"]["rates"]["USDZAR"];

    $shopify = $settings["shopify"];

    $this->config = array(
      'ShopUrl' => $shopify['shop'],
      'ApiKey' => $shopify['api_key'],
      'Password' => $shopify['api_password'],
    );
    
    $this->shopify = ShopifySDK::config($this->config);
    $this->productCount = $this->shopify->Product->count();
    $this->totalPages = ceil($this->productCount/$this->limit);
  
  }

  public function handle()
  {  
    $this->database->getReference('shopify/products/')->remove();
    while ($this->page <= $this->totalPages) {

      $products = $this->getProducts(["page" => $this->page, "limit" => $this->limit]);
      if(count($products) > 0) {
        $this->pushToFirbase($products);
      }
      $this->page++;
    }
    
  }

  function getProducts($params = ["page" => 1, "limit" => 50]) 
  {
    $oberlo_products = [];
    $page = $params['page'];
    $client = new Client();

    try 
    {
      $credentials = base64_encode( $this->config['ApiKey'] . ':' .$this->config['Password']);
      $res = $client->request('GET', 'https://'. $this->config['ShopUrl'].'/admin/products.json',
        [
          'headers' => [
            'Authorization' => ['Basic '. $credentials]
          ],
          'query' => [
            'page' => $params['page'],
            'limit' => $params['limit']
          ]
        ]
      );
          
      $productResponse = json_decode($res->getBody()->getContents(), true);

      if(!isset($productResponse)) return;

      $products = $productResponse['products'];

      foreach ($products as $key => $product) {

        $valid_variant_number_count = 0;
  
        foreach ($product['variants'] as $key => $variant) {
          if($variant['inventory_quantity'] >= 10 ) {
            $valid_variant_number_count++;
          } else {
            unset($product['variants'][$key]);
          }
        }

        if(strpos(strtolower($product['tags']), 'oberlo_') === false) continue;
        if(count($product['images']) < 2) continue;
        if(count($product['variants']) === 0) continue;
        if($valid_variant_number_count === 0) continue;
        
        $oberlo_products[] = $product;

      }

      
    } 
    
    catch (\GuzzleHttp\Exception\ClientException $exception) {}

    catch (\GuzzleHttp\Exception\RequestException $exception)
    {

      $this->getProducts(['page'=> $params['page'], 'limit' => $params['limit'] ]);

    }

    return $oberlo_products;

  }

  function getSampleProducts()
  {
    $data = new \SampleData;
    $products = $data->sample()['products'];
    $filteredProduts = [];

    foreach ($products as $key => $product) {

      $valid_variant_number_count = 0;

      foreach ($product['variants'] as $key => $variant) {
        if($variant['inventory_quantity'] >= 10 ) {
          $valid_variant_number_count++;
        } else {
          unset($product['variants'][$key]);
        }
      }

      if(strpos($product['tags'], 'oberlo_') === false) continue;
      if(count($product['images']) < 2) continue;
      if(count($product['variants']) === 0)  continue;
      if($valid_variant_number_count === 0) continue;

      $filteredProduts[] = $product;

    }

      return $filteredProduts;

  }
  
  function pushToFirbase($products) 
  {
    $oberlo_products_array = [];
    
    foreach ($products as $key => $product) {
      $oberlo_products = [];
      $category_id = 0;
      $additional_images = [];
      $tags = explode(',',$product['tags']);

      foreach($tags as $tag) {
        if(! strpos(strtolower($tag), 'oberlo_')) continue;
        $temp_category_id = explode('[',explode(']',$tag)[0])[1];
        if(is_numeric($temp_category_id)) $category_id = (int) $temp_category_id;
      }

      foreach($product['images'] as $productImage) {
        $additional_images[] = $productImage['src'];
      }
      
      foreach($product['variants'] as $variant) {
        
        $structure = [
          "parent_sku" => (string) $product['id'],
          "sku" => $variant['sku'],
          "price" => number_format($variant['price'] / $this->rate, 2),
          "stock" => $variant['inventory_quantity'],
          "weight" => $variant['weight'],
          "main_image" => $product['image']['src'],
          "published" => false,
          "parent_product" => [
            "title" => $product['title'],
            "category_id" => $category_id,
            "description" => $product['body_html'],
            "main_image" => $product['image']['src'],
            "additional_images" => $additional_images
          ]
        ];

        $oberlo_products_array[] = $structure;
        
        $reference = $this->database->getReference('shopify/products/'. $variant['sku']);
        $snapshot = $reference->getSnapshot();

        $reference->set($structure);

      }
      
    }

    return $oberlo_products_array;
    
  }


}
