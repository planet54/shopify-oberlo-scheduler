<?php

namespace App\Events;

use GuzzleHttp\Client;
use App\Scheduler\Event;

class OberloProductsSyncEvent extends Event
{

  private $oberlo;
  protected $container;
  protected $datebase;

  function __construct($container)
  {
    $this->container = $container;
    $settings = $container->get('settings');
    $firebase = $this->container->get('firebase');
    $this->database = $firebase->getDatabase();
    $this->oberlo = $settings["oberlo"];

  }

  public function handle() 
  {

    $this->updateVariants();
    
  }

  function updateVariants()
  {
    $reference = $this->database->getReference('shopify/products/');
    $products = $reference->getSnapshot()->getValue();

    foreach ($products as $key => $product) {

      $productVariant = $this->getProductVariant($product["sku"]);
      
      if($productVariant !== false) {
        $this->updateProductVariant($product);
      } else {
        $this->createProductVariant($product);
      }

    }

  }

  function getProductVariant(string $sku) 
  {
    $client = new Client();
    try 
    {

      $res = $client->request('GET', 'https://supply.oberlo.com/supplier/api/v1/product-variants/' . $sku ,
        [
          'headers' => [
            'Authorization' => ['Bearer '. $this->oberlo],
            'Accept' => 'application/json',
            'Content-Type' => 'application/json'
          ]
        ]
      );
          
      $oberloResponse = json_decode($res->getBody()->getContents(), true);

      if(!isset($oberloResponse) || $oberloResponse['success'] === false || $oberloResponse === null) return false;
  
      return $oberloResponse['data'];
      
    } 
    
    catch (\GuzzleHttp\Exception\ClientException $exception) 
    {
      return false;
    }

    catch (\GuzzleHttp\Exception\RequestException $exception)
    {

      return $this->getProductVariant();

    }
  }
  function createProductVariant(array $variant)
  {
    $client = new Client();
    try 
    {

      $res = $client->request('POST', 'https://supply.oberlo.com/supplier/api/v1/product-variants',
        [
          'headers' => [
            'Authorization' => ['Bearer '. $this->oberlo],
            'Accept' => 'application/json',
            'Content-Type' => 'application/json'
          ],
          'body' => json_encode($variant)
        ]
      );
          
      $oberloResponse = json_decode($res->getBody()->getContents(), true);

      if(!isset($oberloResponse) || $oberloResponse['success'] === false || $oberloResponse === null) return false;
      //var_dump($oberloResponse['success']);
      return $oberloResponse['data'];
      
    } 
    
    catch (\GuzzleHttp\Exception\ClientException $exception) 
    {
      return false;
    }

    catch (\GuzzleHttp\Exception\RequestException $exception)
    {

      return $this->getProductVariant();

    }
  
  }
  function updateProductVariant(array $variant)
  {
    $client = new Client();
    try 
    {

      $res = $client->request('PUT', 'https://supply.oberlo.com/supplier/api/v1/product-variants/' . $variant["sku"],
        [
          'headers' => [
            'Authorization' => ['Bearer '. $this->oberlo],
            'Accept' => 'application/json',
            'Content-Type' => 'application/json'
          ],
          'body' => json_encode($variant)
        ]
      );
          
      $oberloResponse = json_decode($res->getBody()->getContents(), true);

      if(!isset($oberloResponse) || $oberloResponse['success'] === false || $oberloResponse === null) return false;
      //var_dump($oberloResponse['success']);
      return $oberloResponse['data'];
      
    } 
    
    catch (\GuzzleHttp\Exception\ClientException $exception) 
    {
      return false;
    }

    catch (\GuzzleHttp\Exception\RequestException $exception)
    {

      return $this->getProductVariant();

    }
  }

}
