<?php

namespace App\Scheduler;

use App\Scheduler\Frequencies;
use Carbon\Carbon;
use Cron\CronExpression;

abstract class Event {

  use Frequencies;

  /**
   *  The cron expression for this event.
   *
   * @var string
   */
  public $expression = '* * * * *';

  abstract public function __construct($container);

  /**
   * Handle the event.
   *
   * @return void
   */
  abstract public function handle();

  public function isDueToRun(Carbon $date) {
    
    return CronExpression::factory($this->expression)->isDue($date);

  }

}