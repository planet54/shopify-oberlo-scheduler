<?php

use App\Scheduler\Kernel;
use Carbon\Carbon;

use App\Events\UpdateRatesEvent;
use App\Events\ShopifyProductsSyncEvents;
use App\Events\OberloProductsSyncEvent;
use App\Events\SyncOrdersEvent;
use App\Events\OberloLowInventoryProductsSyncEvent;

require_once 'vendor/autoload.php';
require_once 'config/container.php';

$kernel = new Kernel;

$kernel->add(new UpdateRatesEvent($container))->everyMinute();
$kernel->add(new ShopifyProductsSyncEvents($container))->everyTenMinutes();
$kernel->add(new SyncOrdersEvent($container))->everyThirtyMinutes();

$kernel->run();